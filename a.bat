@echo off
setlocal
if "%~1"=="" goto defaultseed
if not "%~2"=="-novis" goto vis
set fn=testcase%~1.txt
if not exist %fn% (
    echo gen
    java -jar tester.jar -exec "Save.exe %~1" -seed %* >nul
)
Main.exe novis < %fn%
@GOTO finally
:defaultseed
set fn=testcase1.txt
if not exist %fn% (
    echo gen
    java -jar tester.jar -exec "Save.exe 1" -seed 1 >nul
)
Main.exe novis < %fn%
@GOTO finally
:vis
java -jar tester.jar -exec "Main.exe" -seed %*
@GOTO finally
:finally
endlocal