#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cmath>
#include <random>

#ifdef _MSC_VER
#include <chrono>
inline auto get_time() {
	return std::chrono::system_clock::now();
}
inline auto to_msec(int t) {
	return std::chrono::milliseconds(t);
}
using Time = std::chrono::system_clock::time_point;
#else
const double ticks_per_sec = 2800000000;
inline double get_time() {
	uint32_t lo, hi;
	asm volatile ("rdtsc" : "=a" (lo), "=d" (hi));
	return (((uint64_t)hi << 32) | lo) / ticks_per_sec;
}
inline double to_msec(int t) {
	return double(t) / 1000.0;
}
using Time = double;
#endif

using namespace std;

using VI = vector<int>;
using Tp = pair<int, int>;

class MapRecoloring {
    int H, W, C, R;
    int adjMax, IC;
    vector<VI> adjrs;
    vector<vector<Tp> > colist;
    bool *adj;
    VI rcl, losts;
    mt19937 gen;
    int randNext(int x) { return gen() % x; }
    bool dfs(VI&, int);
    void fixIt(VI&, int);
    double calc(VI&, int);
    void idea2(VI&);
    void shuffle(VI&);
    void fit(VI&);
    void permute(VI&);
    void simAnnealing(VI&, Time);
public:
    MapRecoloring() : gen(19831983) {}
    VI recolor(int, VI, VI);
};

VI MapRecoloring::recolor(int H, VI regions, VI oldColors) {
    auto time0 = get_time() + to_msec(9800);
    auto time5 = time0 - to_msec(5800);
    
    this->H = H;
    W = regions.size() / H;
    R = 0;
    for (auto it = regions.begin(); it != regions.end(); it++) {
        R = max(R, *it);
    }
    R++;
    
    adj = new bool[R * R];
    fill(adj, adj + (R * R), false);
    
    for (int row = 0; row < H; row++) {
        for (int col = 0; col < W - 1; col++) {
            int r1 = regions[row * W + col];
            int r2 = regions[row * W + (col + 1)];
            if (r1 == r2) { continue; }
            adj[r1 * R + r2] = true;
            adj[r2 * R + r1] = true;
        }
    }
    
    for (int row = 0; row < H - 1; row++) {
        for (int col = 0; col < W; col++) {
            int r1 = regions[row * W + col];
            int r2 = regions[(row + 1) * W + col];
            if (r1 == r2) { continue; }
            adj[r1 * R + r2] = true;
            adj[r2 * R + r1] = true;
        }
    }
    
    adjrs.resize(R);
    for (int i = 0; i < R; i++) {
        for (int j = 0; j < R; j++) {
            if (adj[i * R + j]) {
                adjrs[i].push_back(j);
            }
        }
        adjMax = max(adjMax, int(adjrs[i].size()));
    }
    C = adjMax;
    
    IC = 0;
    for (auto it = oldColors.begin(); it != oldColors.end(); it++) {
        IC = max(IC, *it);
    }
    IC++;
    
    rcl.resize(C * R);
    losts.resize(C * R);
    for (int row = 0; row < H; row++) {
        for (int col = 0; col < W; col++) {
            int r = regions[row * W + col];
            int c = oldColors[row * W + col];
            rcl[c * R + r] += 1;
        }
    }
    colist.resize(R);
    for (int r = 0; r < R; r++) {
        int sum = 0;
        for (int c = 0; c < C; c++) {
            sum += rcl[c * R + r];
        }
        for (int c = 0; c < C; c++) {
            losts[c * R + r] = sum - rcl[c * R + r];
            if (c < IC) {
                colist[r].push_back({losts[c * R + r], c});
            }
        }
        sort(colist[r].begin(), colist[r].end());
    }
    
    VI ret(R);
    for (int i = 0; i < R; i++) {
        ret[i] = -1;
    }
    bool ok = dfs(ret, 0);
    if (!ok) {
        for (int i = 0; i < R; i++) {
            ret[i] = i;
        }
        return ret;
    }
    fixIt(ret, C);
    int tc = 0;
    for (auto it = ret.begin(); it != ret.end(); it++) {
        tc = max(tc, *it);
    }
    C = min(C, tc + 1);
    auto score = calc(ret, C);
    cerr << "sc: " << score << endl;
    VI tmp(R);
    for (int i = 0; i < R; i++) {
        tmp[i] = ret[i];
    }
    VI tmp2(R);
    idea2(tmp2);
    tc = 0;
    for (auto it = tmp2.begin(); it != tmp2.end(); it++) {
        tc = max(tc, *it);
    }
    tc++;
    auto score2 = calc(tmp2, tc);
    if (score2 > score) {
        swap(ret, tmp2);
        score = score2;
    }
    
    for (int i = 0; ; i++) {
        auto time1 = get_time();
        if (time1 > time5) {
            cerr << "cycles: " << i << endl;
            break;
        }
        this->shuffle(tmp);
        fixIt(tmp, C);
        tc = 0;
        for (auto it = tmp.begin(); it != tmp.end(); it++) {
            tc = max(tc, *it);
        }
        C = min(C, tc + 1);
        auto tmpScore = calc(tmp, C);
        if (tmpScore > score) {
            score = tmpScore;
            for (int j = 0; j < R; j++) {
                ret[j] = tmp[j];
            }
        }
    }
    cerr << "sc: " << score << endl;
    
    fit(ret);
    if (C < 8 || (C == 8 && R < 500)) {
        permute(ret);
    }
    simAnnealing(ret, time0);
    
    return ret;
}

double MapRecoloring::calc(VI& ret, int c) {
    int sc = 0;
    for (int i = 0; i < R; i++) {
        
        sc += losts[ret[i] * R + i];
    }
    return 1.0 - double(c * sc) / double(adjMax * H * W);
}

bool MapRecoloring::dfs(VI& ret, int i) {
    if (i >= ret.size()) {
        return true;
    }
    for (int e = 0; e < C; e++) {
        bool ok = true;
        for (auto it = adjrs[i].begin(); it != adjrs[i].end(); it++) {
            if (ret[*it] == e) {
                ok = false;
                break;
            }
        }
        if (ok) {
            ret[i] = e;
            ok = dfs(ret, i + 1);
            if (ok) {
                return true;
            }
            ret[i] = -1;
        }
    }
    return false;
}

void MapRecoloring::shuffle(VI& ret) {
    for (int i = 0; i < R; i++) {
        for (int k = 0; k < 6; k++) {
            int e = min(randNext(C),
                min(randNext(C),
                    randNext(C)));
            bool ok = true;
            for (auto it = adjrs[i].begin(); it != adjrs[i].end(); it++) {
                if (ret[*it] == e) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                ret[i] = e;
                break;
            }
        }
    }
}

void MapRecoloring::idea2(VI& ret) {
    for (int i = 0; i < R; i++) {
        ret[i] = colist[i][0].second;
    }
    auto wf = new bool[R];
    fill(wf, wf + R, false);
    for (int i = 0; i < R; i++) {
        for (int j = i + 1; j < R; j++) {
            if (!adj[i * R + j]) { continue; }
            if (ret[i] == ret[j]) {
                wf[i] = true;
                wf[j] = true;
            }
        }
    }
    vector<Tp> ws;
    for (int i = 0; i < R; i++) {
        if (wf[i]) {
            ws.push_back({-colist[i][0].first, i});
        }
    }
    sort(ws.begin(), ws.end());
    int mc = IC;
    for (auto e = ws.begin(); e != ws.end(); e++) {
        int i = e->second;
        int c = ret[i];
        bool ok = true;
        auto fg = new bool[mc];
        fill(fg, fg + mc, false);
        int fc = 1;
        fg[c] = true;
        for (auto it = adjrs[i].begin(); it != adjrs[i].end(); it++) {
            int j = *it;
            if (c == ret[j]) {
                ok = false;
            }
            if (fg[ret[j]]) { continue; }
            fg[ret[j]] = true;
            fc++;
            if (fc == mc) { break; }
        }
        if (ok) { continue; }
        for (int k = 1; k < colist[i].size(); k++) {
            if (!fg[colist[i][k].second]) {
                ok = true;
                ret[i] = colist[i][k].second;
            }
        }
        if (ok) { continue; }
        for (int x = IC; x < mc; x++) {
            if (!fg[x]) {
                ok = true;
                ret[i] = x;
                break;
            }
        }
        if (ok) { continue; }
        ret[i] = mc;
        mc++;
    }
    
    fixIt(ret, mc);
    
}

void MapRecoloring::fixIt(VI& ret, int mc) {
    for (int u = 0; u < 100; u++) {
        int uc = 0;
        for (int i = 0; i < R; i++) {
            for (auto ce = colist[i].begin(); ce != colist[i].end(); ce++) {
                int c = ce->second;
                bool ok = true;
                for (auto it = adjrs[i].begin(); it != adjrs[i].end(); it++) {
                    if (c == ret[*it]) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    if (ret[i] == c) { break; }
                    uc++;
                    ret[i] = c;
                    break;
                }
            }
            if (ret[i] < IC) { continue; }
            for (int c = IC; c < mc; c++) {
                bool ok = true;
                for (auto it = adjrs[i].begin(); it != adjrs[i].end(); it++) {
                    if (c == ret[*it]) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    if (ret[i] == c) { break; }
                    uc++;
                    ret[i] = c;
                    break;
                }
            }
        }
        if (uc == 0) { break; }
    }
}


void MapRecoloring::simAnnealing(VI& ret, Time time0) {
    int maxC = C;
    VI tmp(R);
    for (int i = 0; i < R; i++) {
        tmp[i] = ret[i];
    }
    
    int pValue = 0;
    for (int i = 0; i < R; i++) {
        pValue += losts[tmp[i] * R + i];
    }
    int maxPValue = pValue;
    
    for (long long i = 0; ; i++) {
        auto time1 = get_time();
        if (time1 > time0) {
            cerr << "SA: " << i << endl;
            return;
        }
        int p = randNext(R);
        int c = randNext(maxC);
        int b = tmp[p];
        if (b == c) { continue; }
        bool cf = false;
        for (auto it = adjrs[p].begin(); it != adjrs[p].end(); it++) {
            if (tmp[*it] == c) {
                cf = true;
                break;
            }
        }
        if (cf) { continue; }
        int df = losts[c * R + p] - losts[b * R + p];
        if (df >= 0) {
            int rd = randNext(10000);
            if (i < 1000LL) {
                if (df * 100 > pValue * 45) {
                    continue;
                }
                if (rd < 9000) {
                    continue;
                }
            } else if (i < 10000LL) {
                if (df * 100 > pValue * 40) {
                    continue;
                }
                if (rd < 9500) {
                    continue;
                }
            } else if (i < 100000LL) {
                if (df * 100 > pValue * 35) {
                    continue;
                }
                if (rd < 9900) {
                    continue;
                }
            } else if (i < 1000000LL) {
                if (df * 100 > pValue * 30) {
                    continue;
                }
                if (rd < 9990) {
                    continue;
                }
            } else {
                if (df * 100 > pValue * 25) {
                    continue;
                }
                if (rd < 9995) {
                    continue;
                }
            }
        }
        tmp[p] = c;
        pValue += df;
        if (pValue < maxPValue) {
            for (int j = 0; j < R; j++) {
                ret[j] = tmp[j];
            }
            maxPValue = pValue;
        }
    }
    
}

void MapRecoloring::fit(VI&) {
    
}

void MapRecoloring::permute(VI&) {
    
}
