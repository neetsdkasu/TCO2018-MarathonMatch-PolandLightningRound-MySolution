Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System.Collections.Generic
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim args() As String = Environment.GetCommandLineArgs()
            
            Dim vis As Boolean = True
            If args.Count > 1 Then
                vis = False
            End If
            Dim reg As Integer = 0
			Dim H As Integer = CInt(Console.ReadLine())
			Dim S As Integer = CInt(Console.ReadLine())
			Dim regions(S - 1) As Integer
			For i As Integer = 0 To UBound(regions)
				regions(i) = CInt(Console.ReadLine())
                reg = Math.Max(reg, regions(i))
			Next i
            reg += 1
			Dim R As Integer = CInt(Console.ReadLine())
			Dim oldColors(R - 1) As Integer
			For i As Integer = 0 To UBound(oldColors)
				oldColors(i) = CInt(Console.ReadLine())
			Next i
			
            Dim time0 As Integer = Environment.TickCount
			Dim mr As New MapRecoloring()
			Dim ret() As Integer = mr.recolor(H, regions, oldColors)
            Dim time1 As Integer = Environment.TickCount
			
            If vis Then
                Console.WriteLine(ret.Length)
                For Each x As Integer In ret
                    Console.WriteLine(x)
                Next x
                Console.Out.Flush()
            Else
                Dim timediff As Integer = time1 - time0
                time0 += 10000
                Dim timeout As Boolean = time1 >= time0
                Console.WriteLine("time: {0} msec", timediff)
                If timeout Then
                    Console.WriteLine("time out!")
                End If
                If ret Is Nothing Then
                    Console.WriteLine("ret is Nothing")
                    Console.WriteLine("Score = {0}", 0.0)
                End If
                If ret.Count <> reg Then
                    Console.WriteLine("ret size is not correct: reg->{0} ret->{1}", reg, ret.Count)
                    Console.WriteLine("Score = {0}", 0.0)
                End If
                Dim W As Integer = S \ H
                Dim flag(reg - 1, reg - 1) As Boolean
                For row As Integer = 0 To H - 1
                    For col As Integer = 0 To W - 2
                        Dim r1 As Integer = regions(row * W + col)
                        Dim r2 As Integer = regions(row * W + (col + 1))
                        If r1 <> r2 Then
                            If ret(r1) = ret(r2) Then
                                Console.WriteLine("regions {0} and {1} are same color", r1, r2)
                                Console.WriteLine("Score = {0}", 0.0)
                                Exit Sub
                            End If
                            flag(r1, r2) = True
                            flag(r2, r1) = True
                        End If
                    Next col
                Next row
                For row As Integer = 0 To H - 2
                    For col As Integer = 0 To W - 1
                        Dim r1 As Integer = regions(row * W + col)
                        Dim r2 As Integer = regions((row + 1) * W + col)
                        If r1 <> r2 Then
                            If ret(r1) = ret(r2) Then
                                Console.WriteLine("regions {0} and {1} are same color", r1, r2)
                                Console.WriteLine("Score = {0}", 0.0)
                                Exit Sub
                            End If
                            flag(r1, r2) = True
                            flag(r2, r1) = True
                        End If
                    Next col
                Next row
                Dim colors As Integer = 0
                For i As Integer = 0 To reg - 1
                    Dim cnt As Integer = 0
                    For j As Integer = 0 To reg - 1
                        If flag(i, j) Then
                            cnt += 1
                        End If
                    Next j
                    colors = Math.Max(colors, cnt)
                Next i
                Dim diff As Integer = 0
                Dim hs As New HashSet(Of Integer)()
                For i As Integer = 0 To UBound(ret)
                    hs.Add(ret(i))
                Next i
                For i As Integer = 0 To UBound(regions)
                    If ret(regions(i)) <> oldColors(i) Then
                        diff += 1
                    End If
                Next i
                Console.WriteLine("H:{0} W:{1} Reg:{2} MaxJ:{3} ", H, W, reg, colors)
                Console.WriteLine("Diff: {0} / {1}", diff, H * W)
                Console.WriteLine("Color: {0}", hs.Count)
                Dim score As Double = 1.0 - CDbl(diff * hs.Count) / CDbl(H * W * colors)
                If timeout Then
                    Console.WriteLine("score: {0}", score)
                    Console.WriteLine("Score = {0}", 0.0)
                Else
                    Console.WriteLine("Score = {0}", score)
                End If
            End If
                
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module