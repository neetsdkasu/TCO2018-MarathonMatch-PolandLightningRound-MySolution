Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports System.IO
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim args() As String = Environment.GetCommandLineArgs()
            
            Dim seed As Integer = 0
            If args.Length = 2 Then
                seed = CInt(args(1))
            End If
            Dim fileName As String = "testcase" + CStr(seed) + ".txt"
            
            Dim reg As Integer = 0
            
            Using writer As StreamWriter = File.CreateText(fileName)
                Dim H As Integer = CInt(Console.ReadLine())
                writer.WriteLine(H)
                Dim S As Integer = CInt(Console.ReadLine())
                writer.WriteLine(S)
                Dim regions(S - 1) As Integer
                For i As Integer = 0 To UBound(regions)
                    regions(i) = CInt(Console.ReadLine())
                    writer.WriteLine(regions(i))
                    reg = Math.Max(reg, regions(i))
                Next i
                Dim R As Integer = CInt(Console.ReadLine())
                writer.WriteLine(R)
                Dim oldColors(R - 1) As Integer
                For i As Integer = 0 To UBound(oldColors)
                    oldColors(i) = CInt(Console.ReadLine())
                    writer.WriteLine(oldColors(i))
                Next i
            End Using
			
			Console.WriteLine(reg + 1)
			For i As Integer = 0 To reg
				Console.WriteLine(i)
			Next i
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module