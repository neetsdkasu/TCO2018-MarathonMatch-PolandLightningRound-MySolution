Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)

Public Class MapRecoloring
	Dim rand As New Random(19831983)
	Dim H As Integer, W As Integer, R As Integer, C As Integer
    Dim adj(,) As Boolean, adjrs() As List(Of Integer)
    Dim rcl(,) As Integer, losts(,) As Integer
    Dim colist() As List(Of Tp), IC As Integer
Dim adjMax As Integer = 0
	Public Function recolor(H As Integer, regions() As Integer, oldColors() As Integer) As Integer()
		Dim time0 As Integer = Environment.TickCount + 9800
		Dim time5 As Integer = time0 - 5800
		Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))

        Me.H = H
        Me.W = regions.Length \ H
        For Each r As Integer In regions
            Me.R = Math.Max(Me.R, r)
        Next r
        Me.R += 1
        ReDim adj(Me.R - 1, Me.R - 1)
        For row As Integer = 0 To H - 1
            For col As Integer = 0 To W - 2
                Dim r1 As Integer = regions(row * W + col)
                Dim r2 As Integer = regions(row * W + (col + 1))
                If r1 <> r2 Then
                    adj(r1, r2) = True
                    adj(r2, r1) = True
                End If
            Next col
        Next row
        For row As Integer = 0 To H - 2
            For col As Integer = 0 To W - 1
                Dim r1 As Integer = regions(row * W + col)
                Dim r2 As Integer = regions((row + 1) * W + col)
                If r1 <> r2 Then
                    adj(r1, r2) = True
                    adj(r2, r1) = True
                End If
            Next col
        Next row
        ReDim adjrs(Me.R - 1)
        For i As Integer = 0 To Me.R - 1
            adjrs(i) = New List(Of Integer)()
            For j As Integer = 0 To Me.R - 1
                If adj(i, j) Then
                    adjrs(i).Add(j)
                End If
            Next j
            adjMax = Math.Max(adjMax, adjrs.Count)
            ' Console.Error.WriteLine("adjcnt {0} -> {1}", i, adjrs(i).Count)
        Next i
        Me.C = adjMax
        IC = 0
        For Each c As Integer In oldColors
            IC = Math.Max(IC, c)
        Next c
        IC += 1
        Redim rcl(Me.R - 1, Me.C - 1)
        Redim losts(Me.R - 1, Me.C - 1)
        For row As Integer = 0 To H - 1
            For col As Integer = 0 To W - 1
                Dim r As Integer = regions(row * W + col)
                Dim c As Integer = oldColors(row * W + col)
                rcl(r, c) += 1
            Next col
        Next row
        ReDim colist(Me.R - 1)
        For r As Integer = 0 To Me.R - 1
            Dim sum As Integer = 0
            For c As Integer = 0 To Me.C - 1
                sum += rcl(r, c)
            Next c
            colist(r) = New List(Of Tp)()
            For c As Integer = 0 To Me.C - 1
                losts(r, c) = sum - rcl(r, c)
                If c < IC Then
                    colist(r).Add(New Tp(losts(r, c), c))
                End If
                ' Console.Error.WriteLine("losts {0}:{1} -> {2}", r, c, losts(r, c))
            Next c
            colist(r).Sort()
        Next r
        
		Dim ret(Me.R - 1) As Integer
		For i As Integer = 0 To UBound(ret)
            ret(i) = -1
        Next i
        Dim ok As Boolean = Dfs(ret, 0)
        If Not ok Then
            For i As Integer = 0 To UBound(ret)
                ret(i) = i
            Next i
            recolor = ret
            Exit Function
        End If
        FixIt(ret, Me.C)
        Dim tc As Integer = 0
        For Each c As Integer In ret
            tc = Math.Max(tc, c)
        Next c
        Me.C = Math.Min(Me.C, tc + 1)
        Dim score As Double = Calc(ret, Me.C)
        Console.Error.WriteLine("sc: {0}", score)
        Dim tmp(Me.R - 1) As Integer
        Array.Copy(ret, tmp, tmp.Count)
        Dim tmp2() As Integer = Idea2()
        tc = 0
        For Each c As Integer In tmp2
            tc = Math.Max(tc, c)
        Next c
        tc += 1
        Dim score2 As Double = Calc(tmp2, tc)
        If score2 > score Then
            ret = tmp2
            score = score2
        End If
        
        For i As Integer = 0 To Integer.MaxValue
            Dim time1 As Integer = Environment.TickCount
            If time1 > time5 Then
                Console.Error.WriteLine("cycles: {0}", i)
                Exit For
            End If
            Shuffle(tmp)
            FixIt(tmp, Me.C)
            tc = 0
            For Each c As Integer In tmp
                tc = Math.Max(tc, c)
            Next c
            Me.C = Math.Min(Me.C, tc + 1)
            Dim tmpScore As Double = Calc(tmp, Me.C)
            If tmpScore > score Then
                score = tmpScore
                Array.Copy(tmp, ret, ret.Count)
                ' Console.Error.WriteLine("tmpSc: {0}",tmpScore)
            End If
        Next i
        Console.Error.WriteLine("sc: {0}", score)
        
        Fit(ret)
        If Me.C < 8 OrElse (Me.C = 8 AndAlso Me.R < 500) Then
            Permute(ret)
        End IF
        SimAnnealing(ret, time0)
        
		recolor = ret
		
	End Function
    
    Private Function Calc(ret() As Integer, C As Integer) As Double
        Dim score As Integer = 0
        For i As Integer = 0 To UBound(ret)
            score += losts(i, ret(i))
        Next i
        Calc = 1.0 - CDbl(C * score) / CDbl(H * W * adjMax)
    End Function
    
    Private Function Dfs(ret() As Integer, i As Integer) As Boolean
        If i >= ret.Length Then
            Dfs = True
            Exit Function
        End If
        For e As Integer = 0 To C - 1
            Dim ok As Boolean = True
            For Each j As Integer In adjrs(i)
                If ret(j) = e Then
                    ok = False
                    Exit For
                End If
            Next j
            If ok Then
                ret(i) = e
                ok = Dfs(ret, i + 1)
                If ok Then
                    Dfs = True
                    Exit Function
                End If
                ret(i) = -1
            End If
        Next e
        Dfs = False
    End Function
    
    Private Sub Shuffle(ret() As Integer)
        For i As Integer = 0 To UBound(ret)
            For k As Integer = 0 To 5
                Dim e As Integer = Math.Min( _
                    rand.Next(C), Math.Min( _
                    rand.Next(C), rand.Next(C)))
                Dim ok As Boolean = True
                For Each j As Integer In adjrs(i)
                    If ret(j) = e Then
                        ok = False
                        Exit For
                    End If
                Next j
                If ok Then
                    ret(i) = e
                    Exit For
                End If
            Next k
        Next i
    End Sub
    
        
    Private Function Idea2() As Integer()
        
		Dim ret(Me.R - 1) As Integer
		For i As Integer = 0 To UBound(ret)
            ret(i) = colist(i)(0).Item2
        Next i
        
        Dim wf(Me.R - 1) As Boolean
        For i As Integer = 0 To Me.R - 1
            For j As Integer = i + 1 To Me.R - 1
                If Not adj(i, j) Then Continue For
                If ret(i) = ret(j) Then
                    wf(i) = True
                    wf(j) = True
                End If
            Next j
        Next i
        Dim ws As New List(Of Tp)()
        For i As Integer = 0 To UBound(wf)
            If wf(i) Then
                ws.Add(New Tp(-colist(i)(0).Item1, i))
            End If
        Next i
        ws.Sort()
        Dim mc As Integer = IC
        For Each e As Tp In ws
            Dim i As Integer = e.Item2
            Dim c As Integer = ret(i)
            Dim ok As Boolean = True
            Dim fg(mc) As Boolean
            Dim fc As Integer = 1
            fg(c) = True
            For Each j As Integer In adjrs(i)
                If c = ret(j) Then
                    ok = False
                End If
                If fg(ret(j)) Then Continue For
                fg(ret(j)) = True
                fc += 1
                If fc = mc Then Exit For
            Next j
            If ok Then Continue For
            For k As Integer = 1 To colist(i).Count - 1
                If Not fg(colist(i)(k).Item2) Then
                    ok = True
                    ret(i) = colist(i)(k).Item2
                    Exit For
                End If
            Next k
            If ok Then Continue For
            For x As Integer = IC To mc - 1
                If Not fg(x) Then
                    ok = True
                    ret(i) = x
                    Exit For
                End If
            Next x
            If ok Then Continue For
            ret(i) = mc
            mc += 1
        Next e
        
        FixIt(ret, mc)
        
        Idea2 = ret

    End Function
    
        
    Private Sub FixIt(ret() As Integer, mc As Integer)    
    
        For u As Integer = 0 To 100
            Dim uc As Integer = 0
            For i As Integer = 0 To UBound(ret)
                For Each ce As Tp In colist(i)
                    Dim c As Integer = ce.Item2
                    Dim ok As Boolean = True
                    For Each j As Integer In adjrs(i)
                        If c = ret(j) Then
                            ok = False
                            Exit For
                        End If
                    Next j
                    If ok Then
                        If ret(i) = c Then Exit For
                        uc += 1
                        ret(i) = c
                        Exit For
                    End If
                Next ce
                If ret(i) < IC Then Continue For
                For c As Integer = IC To mc - 1
                    Dim ok As Boolean = True
                    For Each j As Integer In adjrs(i)
                        If c = ret(j) Then
                            ok = False
                            Exit For
                        End If
                    Next j
                    If ok Then
                        If ret(i) = c Then Exit For
                        uc += 1
                        ret(i) = c
                        Exit For
                    End If
                Next c
            Next i
            If uc = 0 Then
                Exit For
            End If
            ' Console.WriteLine("u: {0} uc:{1}", u, uc)
        Next u
        
	End Sub
    
    Private Sub Fit(ret() As Integer)
        Dim maxC As Integer = Me.C
        Dim fs(Me.C + 1) As Boolean
        For Each c As Integer In ret
            fs(c) = True
        Next c
        For i As Integer = IC To UBound(fs)
            If Not fs(i) OrElse fs(i - 1) Then Continue For
            Dim es(Me.C + 1) As Integer
            Dim k As Integer = IC
            For j As Integer = IC To UBound(fs)
                If fs(j) Then
                    es(j) = k
                    k += 1
                End If
            Next j
            For j As Integer = 0 To UBound(ret)
                If ret(j) < IC Then Continue For
                ret(j) = es(ret(j))
            Next j
            maxC = k
            Console.Error.WriteLine("fit")
            Exit For
        Next i
        Me.C = maxC
    End Sub
    
    Private Sub SimAnnealing(ret() As Integer, time0 As Integer)
        Dim maxC As Integer = Me.C
        Dim tmp(Me.R - 1) As Integer
        Array.Copy(ret, tmp, tmp.Count)
        
        Dim pValue As Integer = 0
        For i As Integer = 0 To UBound(tmp)
            pValue += losts(i, tmp(i))
        Next i
        Dim maxPValue As Integer = pValue
        
        For i As Long = 0 To Long.MaxValue
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then
                Console.Error.WriteLine("SA: {0}", i)
                Exit Sub
            End If
            Dim p As Integer = rand.Next(Me.R)
            Dim c As Integer = rand.Next(maxC)
            Dim b As Integer = tmp(p)
            If b = c Then Continue For
            Dim cf As Boolean = False
            For Each j As Integer In adjrs(p)
                If tmp(j) = c Then
                    cf = True
                    Exit For
                End If
            Next j
            If cf Then Continue For
            Dim df As Integer = losts(p, c) - losts(p, b)
            If df >= 0 Then
                If i < 1000& Then
                    If df * 100 > pValue * 45 Then
                        Continue For
                    End If
                    If rand.Next(10000) < 9000 Then
                        Continue For
                    End If
                ElseIf i < 10000& Then
                    If df * 100 > pValue * 40 Then
                        Continue For
                    End If
                    If rand.Next(10000) < 9500 Then
                        Continue For
                    End If
                ElseIf i < 100000& Then
                    If df * 100 > pValue * 35 Then
                        Continue For
                    End If
                    If rand.Next(10000) < 9900 Then
                        Continue For
                    End If
                ElseIf i < 1000000& Then
                    If df * 100 > pValue * 30 Then
                        Continue For
                    End If
                    If rand.Next(10000) < 9990 Then
                        Continue For
                    End If
                Else
                    If df * 100 > pValue * 25 Then
                        Continue For
                    End If
                    If rand.Next(10000) < 9995 Then
                        Continue For
                    End If
                End If
            End If
            tmp(p) = c
            pValue += df
            If pValue < maxPValue Then
                Array.Copy(tmp, ret, ret.Count)
                maxPValue = pValue
            End If
        Next i
        
    End Sub
    
    Private Sub Permute(ret() As Integer)
        Dim time0 As Integer = Environment.TickCount
        Dim pm(Me.C - 1) As Integer
        Dim ix(Me.C - 1) As Integer
        For i As Integer = 0 To UBound(pm)
            pm(i) = i
            ix(i) = i
        Next i
        Dim pValue As Integer = 0
        For i As Integer = 0 To UBound(ret)
            pValue += losts(i, ret(i))
        Next i
        Dim cnt As Integer = 0
        Dim f As Boolean = False
        Do
            cnt += 1
            Dim s As Integer = -1
            For i As Integer = UBound(pm) To 1 Step -1
                If pm(i - 1) < pm(i) Then
                    s = i
                    Exit For
                End If
            Next i
            If s < 0 Then Exit Do
            Dim t As Integer = s - 1
            For i As Integer = UBound(pm) To s Step -1
                If pm(i) > pm(t) Then
                    Dim e As Integer = pm(i)
                    pm(i) = pm(t)
                    pm(t) = e
                    Exit For
                End If
            Next i
            Dim lp As Integer = s
            Dim rp As Integer = UBound(pm)
            Do While lp < rp
                Dim e As Integer = pm(lp)
                pm(lp) = pm(rp)
                pm(rp) = e
                lp += 1
                rp -= 1
            Loop
            Dim tmpPValue As Integer = 0
            For i As Integer = 0 To UBound(ret)
                tmpPValue += losts(i, pm(ret(i)))
            Next i
            If tmpPValue < pValue Then
                pValue = tmpPValue
                Array.Copy(pm, ix, ix.Count)
                f = True
            End If
        Loop
        If f Then
            Console.Error.WriteLine("find!")
            For i As Integer = 0 To UBound(ret)
                ret(i) = ix(ret(i))
            Next i
        End If
        Dim time1 As Integer = Environment.TickCount
        Console.Error.WriteLine("Permute: {0}  time: {1} ms", cnt, time1 - time0)
    End Sub
    
End Class