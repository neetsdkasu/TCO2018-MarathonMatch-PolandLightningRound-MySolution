#include "solution.cpp"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <set>

using namespace std;

void solve(bool vis);

int main(int argc, char **argv) {
    
    solve(argc < 2);
    
    return 0;
}

void score(double s) {
    cout << "Score = " << s << endl;
}
    
void solve(bool vis) {
    
    int H, S, R, reg = 0;
    cin >> H >> S;
    vector<int> regions(S);
    for (int i = 0; i < S; i++) {
        cin >> regions[i];
        reg = max(reg, regions[i]);
    }
    reg++;
    cin >> R;
    vector<int> oldColors(R);
    for (int i = 0; i < R; i++) {
        cin >> oldColors[i];
    }
    
    try {
        auto time0 = get_time() + to_msec(10000);
        MapRecoloring mr;
        vector<int> ret = mr.recolor(H, regions, oldColors);
        auto time1 = get_time();
    
        if (vis) {
            cout << ret.size() << endl;
            for (auto it = ret.begin(); it != ret.end(); it++) {
                cout << *it << endl;
            }
            cout.flush();
            return;
        }
        
        bool timeout = false;
        if (time1 > time0) {
            cout << "time out" << endl;
            timeout = true;
        }
        
        if (int(ret.size()) != reg) {
            cout << "ret size is not correct" << endl;
            score(0);
            return;
        }
        
        int W = S / H;
        auto flag = new bool[reg * reg];
        
        for (int row = 0; row < H; row++) {
            for (int col = 0; col < W - 1; col++) {
                int r1 = regions[row * W + col];
                int r2 = regions[row * W + (col + 1)];
                if (r1 == r2) { continue; }
                if (ret[r1] == ret[r2]) {
                    cout << "region same color " << r1 << " and " << r2 << endl;
                    score(0);
                    return;
                }
                flag[r1 * reg + r2] = true;
                flag[r2 * reg + r1] = true;
            }
        }
        
        for (int row = 0; row < H - 1; row++) {
            for (int col = 0; col < W; col++) {
                int r1 = regions[row * W + col];
                int r2 = regions[(row + 1) * W + col];
                if (r1 == r2) { continue; }
                if (ret[r1] == ret[r2]) {
                    cout << "region same color " << r1 << " and " << r2 << endl;
                    score(0);
                    return;
                }
                flag[r1 * reg + r2] = true;
                flag[r2 * reg + r1] = true;
            }
        }
        int maxJ = 0;
        for (int i = 0; i < reg; i++) {
            int cnt = 0;
            for (int j = 0; j < reg; j++) {
                if (flag[i * reg + j]) {
                    cnt++;
                }
            }
            maxJ = max(maxJ, cnt);
        }
        
        set<int> hs;
        for (auto it = ret.begin(); it != ret.end(); it++) {
            hs.insert(*it);
        }
        int diff = 0;
        for (int i = 0; i < S; i++) {
            if (ret[regions[i]] != oldColors[i]) {
                diff++;
            }
        }
        cout << "H: "    << H
             << " W: "    << W
             << " Reg: "  << reg
             << " MaxJ: " << maxJ
             << endl;
             
        cout << "Diff: " << diff << " / " << (H * W) << endl;
        cout << "Colors: " << hs.size() << endl;
        
        double sc = 1.0 - double(hs.size() * diff) / double(maxJ * H * W);
        
        if (timeout) {
            cout << "score: " << sc << endl;
            score(0);
        } else {
            score(sc);
        }
    
    } catch (...) {
        cout << "some exception!" << endl;
        score(0);
    }
    
}